#= require 'labyrinth/util'
#= require 'labyrinth/base_game'
#= require 'labyrinth/game'
#= require 'labyrinth/entity'
#= require_directory './labyrinth/entities'
#= require 'labyrinth/entity_table'
#= require 'labyrinth/map'

window.addEvent 'domready', ->
	window.game = new Game 'labyrinth'
	window.game.start()
