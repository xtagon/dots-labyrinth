window.Map = new Class
	initialize: (@game, @data) ->
		return this

	load: ->
		@unLoad()

		@data.each (row, y) =>
			@grid[y] = []

			for x in [0...row.length / 2]
				@grid[y][x] = null

				entityCode = row.charAt(x * 2)

				if entityCode is entityTable.start
					@start = {x: x, y: y}
				else
					entityType = entityTable[entityCode]

					if entityType
						entityChannel = row.charAt(x * 2 + 1)
						entityChannel = null if entityChannel is entityCode
						options = {position: {x: x, y: y}}
						options.channel = entityChannel if entityChannel
						@grid[y][x] = new entityType @game, options

		return this

	unLoad: ->
		@grid = []

	draw: ->
		@grid.each (row) =>
			row.each (entity) =>
				entity.draw() if entity
