window.Entity = new Class
	Implements: Options

	entityType: "Entity"

	isSolid: yes
	isTool: no

	options:
		position:
			x: 0
			y: 0

	initialize: (@game, options) ->
		@setOptions options

		return this

	draw: ->
		@game.view.save()
		@game.applyCellOffset(@options.position)
		@drawWrapped()
		@game.view.restore()
