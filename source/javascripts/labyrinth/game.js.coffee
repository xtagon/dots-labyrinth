window.Game = new Class
	Extends: BaseGame

	options:
		cellSize: 28
		cellPadding: 4
		colors:
			background: "#121212"
			block: "#343434"
			doorKey:
				"B": "#58D"
				"G": "#5A5"
				"P": "#85B"
				"R": "#C44"
				"Y": "#D4D463"
			goal:
				ring: "#3A3"
				plus: "#3F3"
			planks: "#A77B48"
			player:
				dot: "#FFF"
				ring: "#686868"
				points: "BBB"

	initialize: (options = {}) ->
		console.log "Initializing game"

		@parent(options)

		@player = new Player this
		@fetchMaps()

		@inputElement.addEvent "keydown", (event) =>
			@onKeydown(event)

		return this

	fetchMaps: ->
		console.log "Fetching maps"

		new Request.JSON
			url: "/maps.json"
			link: "cancel"

			onSuccess: (json) =>
				@maps = []
				json.maps.each (mapData, mapNumber) =>
					@maps[mapNumber] = new Map this, mapData

				console.log "Done fetching maps"

				@loadMap()

			onError: ->
				console.log "Error occured while parsing map definition file"

			onFailure: ->
				console.log "Error occured while trying to fetch map data"
		.get()

		return this

	loadMap: (mapNumber = 0) ->
		return false if mapNumber >= @maps.length
		return false if mapNumber < 0

		console.log "Loading map ##{mapNumber}"

		if @map
			console.log "Unloading map ##{@mapNumber}"
			@map.unLoad()

		@mapNumber = mapNumber
		@map = @maps[mapNumber].load()
		@player.options.position = @map.start

		console.log "Map ##{mapNumber} loaded"

		return this

	loadNextMap: ->
		@loadMap @mapNumber + 1

	loadPreviusMap: ->
		@loadMap @mapNumber - 1

	draw: ->
		return true unless @map

		@view.save()
		@drawBackground()
		@applyScrollOffset()
		@map.draw()
		@player.draw()
		@view.restore()

	drawBackground: ->
		@view.fillStyle = @options.colors.background
		@view.fillRect 0, 0, @canvas.width, @canvas.height

	applyCellOffset: (position) ->
		offset = @options.cellSize + @options.cellPadding
		@view.translate(position.x * offset, position.y * offset)

	applyScrollOffset: ->
		playerOffset =
			x: @player.options.position.x * -1
			y: @player.options.position.y * -1
		@applyCellOffset playerOffset

		centerPlayer = @options.cellSize / 2 * -1
		@view.translate(centerPlayer, centerPlayer)

		@view.translate(@canvas.width / 2, @canvas.height / 2)

	getProgress: ->
		@player.points / @maps.length

	onKeydown: (event) ->
		switch event.key
			when "up",    "w", "k" then @player.goNorth()
			when "down",  "s", "j" then @player.goSouth()
			when "left",  "a", "h" then @player.goWest()
			when "right", "d", "l" then @player.goEast()
			when "space" then @player.swapTool()
			else
				if @options.cheat
					switch event.key
						when "n" then @loadNextMap()
						when "p" then @loadPreviusMap()
