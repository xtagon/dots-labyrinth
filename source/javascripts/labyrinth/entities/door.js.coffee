window.Door = new Class
	Extends: Entity

	entityType: "Door"

	drawWrapped: ->
		v = @game.view
		size = @game.options.cellSize
		bottomRadius = size / 10
		topRadius = size / 2

		v.fillStyle = @game.options.colors.doorKey[@options.channel]
		v.beginPath()
		v.moveTo(topRadius, 0)
		v.lineTo(size - bottomRadius, 0)
		v.arcTo(size, 0, size, topRadius, topRadius)
		v.lineTo(size, size - bottomRadius)
		v.arcTo(size, size, size - bottomRadius, size, bottomRadius)
		v.lineTo(bottomRadius, size)
		v.arcTo(0, size, 0, size - bottomRadius, bottomRadius)
		v.lineTo(0, topRadius)
		v.arcTo(0, 0, topRadius, 0, topRadius)
		v.closePath()
		v.fill()

		v.fillStyle = @game.options.colors.background
		v.beginPath()
		v.arc(size * 0.75, size * 0.62, size / 9, 0, Math.PI * 2, false)
		v.closePath()
		v.fill()

	unlock: ->
		key = @game.player.tool

		return false unless key and key.entityType is "Key"
		return false unless key.options.channel is @options.channel

		@game.player.tool = null
		@game.map.grid[@options.position.y][@options.position.x] = null

		return true
