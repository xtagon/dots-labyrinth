window.Planks = new Class
	Extends: Entity

	entityType: "Planks"

	isSolid: no
	isTool: yes

	drawWrapped: ->
		v = @game.view
		size = @game.options.cellSize
		width = size / 4.2
		margin = width / 4

		v.translate(size * 0.51, size * -0.21)
		v.rotate(Math.PI / 4)
		v.strokeStyle = @game.options.colors.planks
		v.lineWidth = width

		x = size / 3.25
		v.beginPath()
		v.moveTo(x, margin)
		v.lineTo(x, size - margin)
		v.closePath()
		v.stroke()

		x = size - x
		v.beginPath()
		v.moveTo(x, margin)
		v.lineTo(x, size - margin)
		v.closePath()
		v.stroke()

	getColor: ->
		@game.options.colors.planks
