window.Key = new Class
	Extends: Entity

	entityType: "Key"

	isSolid: no
	isTool: yes

	drawWrapped: ->
		v = @game.view
		size = @game.options.cellSize
		radius = size * 0.17
		length = size * 0.9

		v.translate(size * 0.82, size * 0.2)
		v.rotate Math.PI / 4
		v.strokeStyle = @game.options.colors.doorKey[@options.channel]

		v.lineWidth = 3
		v.beginPath()
		v.arc(0, radius, radius, 0, Math.PI * 2, false)
		v.closePath()
		v.stroke()

		x = size * 0.02
		v.lineWidth = 3.2
		v.beginPath()
		v.moveTo(x, radius * 2)
		v.lineTo(x, length)
		v.stroke()

		y = length - 1
		v.beginPath()
		v.moveTo(x, y)
		v.lineTo(x - radius, y)
		v.stroke()

		y -= 5
		v.beginPath()
		v.moveTo(x, y)
		v.lineTo(x - radius, y)
		v.stroke()

	getColor: ->
		@game.options.colors.doorKey[@options.channel]
