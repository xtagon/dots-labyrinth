window.Player = new Class
	Extends: Entity

	entityType: "Player"

	initialize: (game, properties) ->
		@parent game, properties
		@points = 0
		@tool = null

		return this

	drawWrapped: ->
		v = @game.view
		colors = @game.options.colors.player
		size = @game.options.cellSize
		halfSize = size / 2
		innerRadius = halfSize * 0.38
		outerRadius = innerRadius * 1.55
		twoPI = Math.PI * 2
		progressAngle = twoPI * @game.getProgress()

		v.translate(halfSize, halfSize)
		v.rotate -Math.PI / 2

		v.fillStyle = colors.dot
		v.beginPath()
		v.arc(0, 0, innerRadius, 0, twoPI, false)
		v.closePath()
		v.fill()

		v.lineWidth = 1.9

		v.strokeStyle = colors.ring
		v.beginPath()
		v.arc(0, 0, outerRadius, progressAngle, twoPI, false)
		v.stroke()

		v.strokeStyle = colors.points
		v.beginPath()
		v.arc(0, 0, outerRadius, 0, progressAngle, false)
		v.stroke()

		if @tool and @tool.isTool
			toolColor = @tool.getColor()
			toolColorRadius = innerRadius * 2.3

			v.lineWidth = 1.5
			v.strokeStyle = toolColor
			v.beginPath()
			v.arc(0, 0, toolColorRadius, Math.PI * 0.25, Math.PI * 0.75, false)
			v.stroke()
			v.beginPath()
			v.arc(0, 0, toolColorRadius, Math.PI * 1.25, Math.PI * 1.75, false)
			v.stroke()

	moveTo: (position) ->
		neighbor = @game.map.grid[position.y][position.x]

		if neighbor
			collision = neighbor.isSolid

			switch neighbor.entityType
				when "Goal"
					@points += 1
					@game.loadNextMap()
				when "Door"
					collision = !neighbor.unlock()

		@options.position = position unless collision

	goNorth: ->
		@moveTo
			x: @options.position.x
			y: @options.position.y - 1

	goSouth: ->
		@moveTo
			x: @options.position.x
			y: @options.position.y + 1

	goWest: ->
		@moveTo
			x: @options.position.x - 1
			y: @options.position.y

	goEast: ->
		@moveTo
			x: @options.position.x + 1
			y: @options.position.y

	swapTool: ->
		toolOnGround = @game.map.grid[@options.position.y][@options.position.x]
		toolInHand = @tool
		toolInHand.options.position = @options.position if toolInHand
		@game.map.grid[@options.position.y][@options.position.x] = toolInHand
		@tool = toolOnGround
