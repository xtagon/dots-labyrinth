window.Goal = new Class
	Extends: Entity

	entityType: "Goal"

	drawWrapped: ->
		v = @game.view
		colors = @game.options.colors.goal
		size = @game.options.cellSize
		halfSize = size / 2
		ringRadius = halfSize * 0.62
		plusRadius = halfSize * 0.32

		v.translate(halfSize, halfSize)

		v.strokeStyle = colors.ring
		v.lineWidth = 2
		v.beginPath()
		v.arc(0, 0, ringRadius, 0, 2 * Math.PI, false)
		v.closePath()
		v.stroke()

		v.strokeStyle = colors.plus
		v.lineWidth = 2
		v.beginPath()
		v.moveTo(0, -plusRadius)
		v.lineTo(0, plusRadius)
		v.closePath()
		v.stroke()
		v.beginPath()
		v.moveTo(-plusRadius, 0)
		v.lineTo(plusRadius, 0)
		v.closePath()
		v.stroke()
