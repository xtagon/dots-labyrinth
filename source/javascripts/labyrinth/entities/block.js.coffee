window.Block = new Class
	Extends: Entity

	entityType: "Block"

	drawWrapped: ->
		v = @game.view

		size = @game.options.cellSize
		radius = size / 6

		v.fillStyle = @game.options.colors.block
		v.beginPath()
		v.moveTo(radius, 0)
		v.lineTo(size - radius, 0)
		v.arcTo(size, 0, size, radius, radius)
		v.lineTo(size, size - radius)
		v.arcTo(size, size, size - radius, size, radius)
		v.lineTo(radius, size)
		v.arcTo(0, size, 0, size - radius, radius)
		v.lineTo(0, radius)
		v.arcTo(0, 0, radius, 0, radius)
		v.closePath()
		v.fill()
