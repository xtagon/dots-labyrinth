window.BaseGame = new Class
	Implements: Options

	options:
		autopause: yes
		fullscreen: yes
		cheat: off

	initialize: (canvas, options = {}) ->
		@setOptions options

		@canvas = $(canvas)
		@view = @canvas.getContext '2d'

		if @options.fullscreen
			@canvas.setStyles
				position: "absolute"
				top: "0"
				left: "0"

			window.addEvent 'resize', =>
				@canvas.width = window.innerWidth
				@canvas.height = window.innerHeight
			window.fireEvent 'resize'

			@inputElement = document
		else
			@inputElement = @canvas
			@inputElement.set('tabindex', 1)
			@inputElement.focus()

		if @options.autopause
			window.addEvents
				blur: => @pause()
				focus: => @resume()

		@options.cheat = on if window.location.hash.match "cheat"

		return this

	start: ->
		return this if @running

		@running = true

		stepCallback = =>
			return this unless @running
			@step()
			requestAnimationFrame stepCallback

		@timeOfLastStep = Date.now()
		requestAnimationFrame stepCallback

		return this

	stop: ->
		@running = false

		return this

	pause: -> @stop()

	resume: -> @start()

	step: ->
		now = Date.now()
		@timeSinceLastStep = (now - @timeOfLastStep)
		@timeOfLastStep = now

		@update()
		@draw()

	update: ->
	
	draw: ->
