#!/bin/sh

BUCKET_NAME="labyrinth.xtagon.com"

bundle exec middleman build --clean && \
s3cmd sync --delete-removed --reduced-redundancy ./build/ "s3://$BUCKET_NAME/" && \
s3cmd du -H "s3://$BUCKET_NAME/"
